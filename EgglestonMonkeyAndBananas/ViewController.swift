//
//  ViewController.swift
//  EgglestonMonkeyAndBananas
//
//  Created by Eggleston,Tyler M on 11/17/16.
//  Copyright © 2016 Eggleston,Tyler M. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var monkeyView: UIImageView!
    @IBOutlet weak var bananaView1: UIImageView!
    @IBOutlet weak var bananaView2: UIImageView!
    @IBOutlet weak var bananaView3: UIImageView!
    @IBOutlet weak var bananaView4: UIImageView!
    
    @IBOutlet weak var ProblemTF: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func handlePan(sender:UIPanGestureRecognizer) {
        let translation = sender.translationInView(self.view)
        if let view = sender.view {
            view.center = CGPoint(x:view.center.x + translation.x,
                                  y:view.center.y + translation.y)
        }
        sender.setTranslation(CGPointZero, inView: self.view)
    }
    var num = 0
    @IBAction func NewProblemBT(sender: AnyObject) {
        num = Int(arc4random_uniform((4)+1))
        ProblemTF.text = "I want \(num) Bananas"
    }
    
    @IBAction func CheckResultBT(sender: AnyObject) {
        var bananasMoved = 0
        if sqrt(pow(monkeyView.center.x - bananaView1.center.x, 2) + pow(monkeyView.center.y - bananaView1.center.y, 2)) <= 80{
            bananasMoved += 1
        }
        
        if sqrt(pow(monkeyView.center.x - bananaView2.center.x, 2) + pow(monkeyView.center.y - bananaView2.center.y, 2)) <= 80{
            bananasMoved += 1
        }
        
        if sqrt(pow(monkeyView.center.x - bananaView3.center.x, 2) + pow(monkeyView.center.y - bananaView3.center.y, 2)) <= 80{
            bananasMoved += 1
        }
        
        if sqrt(pow(monkeyView.center.x - bananaView4.center.x, 2) + pow(monkeyView.center.y - bananaView4.center.y, 2)) <= 80{
            bananasMoved += 1
        }
        
        if num == bananasMoved{
            self.ProblemTF.text = "Thanks :)"
        }
        
        else{
            self.ProblemTF.text = "Try again, I want \(num) bananas"
        }
        
    }
    

}

